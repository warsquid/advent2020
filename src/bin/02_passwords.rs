use std::fs::File;
use std::io::{BufRead, BufReader, Error, ErrorKind, Read};
use std::time::Instant;
use std::convert::TryInto;

#[derive(Debug)]
struct PasswordCase {
    n_min: i64,
    n_max: i64,
    target_letter: char,
    password: String
}

fn read_input<R: Read>(io: R) -> Vec<PasswordCase> {
    let br = BufReader::new(io);
    let passwords = br.lines()
        .map(|s| s.unwrap())
        .map(|line| line.split(' ').map(|x| x.to_string()).collect())
        .map(|items: Vec<String>| {
            let range = &items[0];
            let range_split: Vec<i64> = range.split('-').map(|n| n.parse::<i64>().unwrap()).collect();
            let n_min = range_split[0];
            let n_max = range_split[1];
            let target = items[1].chars().nth(0).unwrap();
            let password = &items[2];
            let password_case = PasswordCase {
                n_min: n_min,
                n_max: n_max,
                target_letter: target,
                password: password.to_string()
            };
            return password_case;
        })
        .collect();
    return passwords
}

fn count_valid_passwords1(cases: &Vec<PasswordCase>) -> usize {
    let results: Vec<usize> = cases.iter()
        .enumerate()
        .filter(|(_index, case)| {
            let target_count: i64 = case.password.chars()
                .filter(|c| c == &case.target_letter)
                .count()
                .try_into()
                .unwrap();
            (target_count <= case.n_max) && (target_count >= case.n_min)
        })
        .map(|(index, _case)| index + 1)
        .collect();
    results.len()
}

fn main() {
    let password_cases = read_input(File::open("data/02_actual.txt").unwrap());
       
    let p1 = count_valid_passwords1(&password_cases);
    // println!("{:?}", results);
    println!("{}", p1);

    let p2_results: Vec<usize> = password_cases.iter()
        .enumerate()
        .filter(|(_index, case)| {
            let pos1_char = case.password.chars().nth(case.n_min as usize - 1).unwrap();
            let pos2_char = case.password.chars().nth(case.n_max as usize - 1).unwrap();
            return (pos1_char == case.target_letter) ^ (pos2_char == case.target_letter)
        })
        .map(|(index, _case)| index + 1)
        .collect();

    println!("{:?}", p2_results);
    println!("{}", p2_results.len());
}



