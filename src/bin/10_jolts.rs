use std::fs::File;
use std::io::{BufRead, BufReader, Read};
use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;

fn read_input<R: Read>(io: R) -> Vec<String> {
    let br = BufReader::new(io);
    let lines = br.lines();
    lines.map(|x| x.unwrap().to_string())
        .collect()
}

fn main() {
    let input = read_input(File::open("data/10_actual.txt").unwrap());

    let outputs: Vec<i64> = input.iter()
        .map(|n| n.parse().expect("Couldn't parse number"))
        .collect();
    // println!("Outputs: {:?}", outputs);

    let ans1 = chain_all(&outputs);
    println!("Part 1: {}", ans1);

    let ans2 = count_all_arrangements(&outputs);
    println!("Part 2: {}", ans2);
}

fn chain_all(outputs: &Vec<i64>) -> i64 {
    let all_outputs: HashSet<i64> = HashSet::from_iter(outputs.iter().cloned()); 

    let max_out = outputs.iter().fold(0, |a, b| if a > *b {a} else {*b}) + 3;

    let (success, one_count, three_count) = chain_all_recurse(&all_outputs, 0, 0, max_out);
    // println!("Ones: {}, Threes: {}", one_count, three_count);
    return one_count * three_count;
}

fn chain_all_recurse(outputs: &HashSet<i64>, n_used: usize, current_out: i64, max_out: i64) -> (bool, i64, i64) {
    if n_used == outputs.len() {

        let diff = max_out - current_out;
        // println!("{}", diff);
        if diff <= 3 {
            match diff {
                1 => {
                    return (true, 1, 0);
                }
                3 => {
                   return (true, 0, 1);
                }
                _ => {
                    return (true, 0, 0);
                }
            };
        } else {
            return (false, 0, 0);
        }
    }

    // println!("Eligible: {:?}", eligible);
    for offset in 1..4 {
        let n = current_out + offset;
        if outputs.contains(&n) {
            let diff = n - current_out;
            let (success, one_count, three_count) = chain_all_recurse(outputs, n_used + 1, n, max_out);
            if success {
                // println!("Success, {}, {}", one_count, three_count);
                if diff == 1 {
                    return (success, one_count + 1, three_count);
                } else if diff == 3 {
                    return (success, one_count, three_count + 1);
                }
            }
        }
    }
    return (false, 0, 0);
}

fn count_all_arrangements(outputs: &Vec<i64>) -> i64 {
    let mut all_outputs = outputs.to_vec();
    all_outputs.sort();

    let max_out = outputs.iter().fold(0, |a, b| if a > *b {a} else {*b}) + 3;
    let mut paths: HashMap<i64, i64> = HashMap::new();
    paths.insert(0, 1);
    for jolt in 0..max_out {
        let current_paths = paths.get(&jolt).cloned().unwrap_or(0);
        for offset in 1..4 {
            let new_jolt = jolt + offset;
            if all_outputs.contains(&new_jolt) || (new_jolt == max_out) {
                // println!("Prev: {}, Current: {}, Paths: {}", jolt, new_jolt, current_paths);
                if paths.contains_key(&new_jolt)  {
                    *paths.get_mut(&new_jolt).unwrap() += current_paths;
                } else {
                    paths.insert(new_jolt, current_paths);
                }
            }
        }
    }

    return paths[&max_out];
}
