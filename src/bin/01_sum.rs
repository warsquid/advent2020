use std::fs::File;
use std::io::{BufRead, BufReader, Error, ErrorKind, Read};
use std::time::Instant;

fn read<R: Read>(io: R) -> Result<Vec<i64>, Error> {
    let br = BufReader::new(io);
    br.lines()
        .map(|line| 
             line.and_then(
                 |v| v.parse().map_err(|e| Error::new(ErrorKind::InvalidData, e))
             )
         )
        .collect()
}

fn find_two(nums: &mut Vec<i64>, target: i64) -> i64 {
    nums.sort();

    let mut left_index = 0;
    let mut right_index = nums.len() - 1;
    let mut found = false;

    while ! found {
        let sum = nums[left_index] + nums[right_index];
        if sum == target {
            found = true;
        } else if sum > target {
            right_index -= 1;
        } else {
            left_index += 1;
        }
    }

    let prod = nums[left_index] * nums[right_index];
    return prod;
}

fn main() -> Result<(), Error> {
    let mut nums = read(File::open("data/01_actual.txt")?)?;

    let start = Instant::now();
    let prod = find_two(&mut nums, 2020);
    dbg!(Instant::now() - start);
    println!("Part 1 answer:");
    println!("{}", prod);

    let mut vec = read(File::open("data/01_actual.txt")?)?;
    vec.sort();
    let target = 2020;
    let mut a_index = 0;
    let mut b_index = 1;
    let mut c_index = vec.len() - 1;
    let mut three_found = false;

    while ! three_found {
        let sum = vec[a_index] + vec[b_index] + vec[c_index];

        if sum == target {
            three_found = true;
        } else if b_index == c_index {
            a_index += 1;
            b_index = a_index + 1;
            c_index = vec.len() - 1;
        } else if sum > target {
            c_index -= 1;
        } else {
            b_index += 1;
        }
    }

    let three_prod = vec[a_index] * vec[b_index] * vec[c_index];
    println!("Part two:");
    println!("a, b, c: {}, {}, {}", a_index, b_index, c_index);
    println!("{}", three_prod);

    return Ok(()); 
}
