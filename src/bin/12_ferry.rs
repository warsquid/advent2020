use std::fs::File;
use std::io::{BufRead, BufReader, Read};

#[derive(Debug, Copy, Clone)]
enum Direction {
    East,
    South,
    West,
    North
}

enum Turn {
    Left,
    Right
}

struct Ferry {
    direction: Direction,
    x: i64,
    y: i64
}

impl Ferry {
    fn sail(&mut self, direction: &Direction, steps: i64) {
        match direction{
            Direction::East =>  self.x += steps,
            Direction::South => self.y -= steps,
            Direction::West => self.x -= steps,
            Direction::North => self.y += steps
        }
    } 

    fn turn(&mut self, turn_direction: &Turn, angle: i64) {
        let n_turns = angle / 90;
        for _turn_num in 0..n_turns {
            match turn_direction {
                Turn::Left => {
                    match self.direction {
                        Direction::East => self.direction = Direction::North,
                        Direction::North => self.direction = Direction::West,
                        Direction::West => self.direction = Direction::South,
                        Direction::South => self.direction = Direction::East
                    }
                }
                Turn::Right => {
                    match self.direction {
                        Direction::East => self.direction = Direction::South,
                        Direction::North => self.direction = Direction::East,
                        Direction::West => self.direction = Direction::North,
                        Direction::South => self.direction = Direction::West
                    }
                }
        }

        }
    }
}

#[derive(Debug)]
struct FerryWithWaypoint {
    ship_x: i64,
    ship_y: i64,
    waypoint_x: i64,
    waypoint_y: i64
}

impl FerryWithWaypoint {
    fn sail(&mut self, steps: i64) {
        self.ship_x += steps * self.waypoint_x;
        self.ship_y += steps * self.waypoint_y;
    }

    fn move_waypoint(&mut self, direction: &Direction, steps: i64) {
        match direction{
            Direction::East =>  self.waypoint_x += steps,
            Direction::South => self.waypoint_y -= steps,
            Direction::West => self.waypoint_x -= steps,
            Direction::North => self.waypoint_y += steps
        }
    }

    fn rotate_waypoint(&mut self, turn_direction: &Turn, angle: i64) {
        let n_turns = angle / 90;
        for _ in 0..n_turns {
            let x = self.waypoint_x;
            let y = self.waypoint_y;
            match turn_direction {
                Turn::Left => {
                    self.waypoint_x = -y;
                    self.waypoint_y = x;
                },
                Turn::Right => {
                    self.waypoint_x = y;
                    self.waypoint_y = -x;
                }
            }
        }
    }
}


fn read_input<R: Read>(io: R) -> Vec<String> {
    let br = BufReader::new(io);
    let lines = br.lines();
    lines.map(|x| x.unwrap().to_string())
        .collect()
}


fn follow_instructions(instructions: &Vec<String>) -> Ferry {
    let mut ferry = Ferry{
        direction: Direction::East, 
        x: 0, y: 0
    };
    for step in instructions {
        let command = step.get(0..1).unwrap();
        let num: i64 = step.get(1..).unwrap().parse().unwrap();
        let current_direction = ferry.direction;

        match command {
            "F" => ferry.sail(&current_direction, num),
            "N" => ferry.sail(&Direction::North, num),
            "E" => ferry.sail(&Direction::East, num),
            "S" => ferry.sail(&Direction::South, num),
            "W" => ferry.sail(&Direction::West, num),
            "L" => ferry.turn(&Turn::Left, num),
            "R" => ferry.turn(&Turn::Right, num),
            _ => println!("Unrecognized command: {}", command)
        }
    }

    return ferry
}

fn follow_waypoint_instructions(instructions: &Vec<String>) -> FerryWithWaypoint {
    let mut ferry = FerryWithWaypoint{
        ship_x: 0, ship_y: 0,
        waypoint_x: 10, waypoint_y: 1
    };
    for step in instructions {
        let command = step.get(0..1).unwrap();
        let num: i64 = step.get(1..).unwrap().parse().unwrap();

        match command {
            "F" => ferry.sail(num),
            "N" => ferry.move_waypoint(&Direction::North, num),
            "E" => ferry.move_waypoint(&Direction::East, num),
            "S" => ferry.move_waypoint(&Direction::South, num),
            "W" => ferry.move_waypoint(&Direction::West, num),
            "L" => ferry.rotate_waypoint(&Turn::Left, num),
            "R" => ferry.rotate_waypoint(&Turn::Right, num),
            _ => println!("Unrecognized command: {}", command)
        }
        // println!("{:?}", ferry);
    }

    return ferry
}

fn main() {
    let instructions = read_input(File::open("data/12_actual.txt").unwrap());
    let p1_ferry = follow_instructions(&instructions);
    println!("Final pos: {},{}", p1_ferry.x, p1_ferry.y);

    let p1_ans = p1_ferry.x.abs() + p1_ferry.y.abs();
    println!("Part 1 answer: {}", p1_ans);

    let p2_ferry = follow_waypoint_instructions(&instructions);
    println!("Final pos: {},{}", p2_ferry.ship_x, p2_ferry.ship_y);
    let p2_ans = p2_ferry.ship_x.abs() + p2_ferry.ship_y.abs();
    println!("Part 2 answer: {}", p2_ans);
}