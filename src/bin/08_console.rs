#![feature(str_split_once)]
use std::fs::File;
use std::io::{BufRead, BufReader, Read};
use std::collections::HashSet;

struct Console {
    index: i64,
    acc: i64,
    instructions: Vec<(String, i64)>
}

impl Console {
    fn step(&mut self) {
        let (command, n) = &self.instructions[self.index as usize];
        match command.as_str() {
            "nop" => {
                self.index += 1;
            }
            "acc" => {
                self.index += 1;
                self.acc += n;
            }
            "jmp" => {
                self.index += n;
            }
            _ => println!("Invalid command: {}", command)
        }
    }
}

fn read_input<R: Read>(io: R) -> Vec<String> {
    let br = BufReader::new(io);
    let lines = br.lines();
    lines.map(|x| x.unwrap().to_string())
        .collect()
}

fn parse_instructions(input: &Vec<String>) -> Vec<(String, i64)> {
    input.iter()
        .map(|s| s.split_once(' ').unwrap())
        .map(|(ins, n)| (ins.to_string(), n.parse().unwrap()))
        .collect()
}

fn do_part_one(instructions: Vec<(String, i64)>) {
    let mut console = Console {
        index: 0,
        acc: 0,
        instructions: instructions
    };

    let mut seen: HashSet<i64> = HashSet::new();
    let mut acc_value = console.acc;
    loop {
        seen.insert(console.index);

        console.step();

        if seen.contains(&console.index) {
            println!("Final acc value: {}", acc_value);
            break;
        }
        acc_value = console.acc;
    }
}

fn do_part_two(instructions: Vec<(String, i64)>) {
    let positions = instructions.iter()
        .enumerate()
        .filter(|(index, inst)| {
            let (command, n) = inst;
            if (command.as_str() == "jmp") || (command.as_str() == "nop") {
                return true;
            } else {
                return false;
            }
        })
        .map(|(index, inst)| index);
    
   for pos in positions {
       let mut inst_copy = instructions.clone();
       let comm_to_swap = &inst_copy[pos].0;
       let new_comm: String = if comm_to_swap == "nop" {"jmp".to_string()} else {"nop".to_string()};
       inst_copy[pos].0 = new_comm.clone();
       // println!("{:?}", inst_copy);

       let (terminates, acc) = test_instructions(inst_copy);
       if terminates {
           println!("Terminated with acc value: {}", acc);
       }
   } 
}

fn test_instructions(instructions: Vec<(String, i64)>) -> (bool, i64) {
    let mut console = Console {
        index: 0,
        acc: 0,
        instructions: instructions
    };

    let mut seen: HashSet<i64> = HashSet::new();
    let mut acc_value = console.acc;
    loop {
        seen.insert(console.index);

        console.step();
        // println!("Index: {}, Acc: {}", console.index, console.acc);

        acc_value = console.acc;
        if seen.contains(&console.index) {
            return (false, acc_value);
        }
        if (console.index >= console.instructions.len() as i64) {
            return (true, acc_value);
        }
    }
}

fn main() {
    let input = read_input(File::open("data/08_actual.txt").unwrap());

    let instructions = parse_instructions(&input);
    // println!("{:?}", instructions);

    do_part_one(instructions);
    
    let instructions_two = parse_instructions(&input);
    // println!("{:?}", instructions);
    do_part_two(instructions_two);
}