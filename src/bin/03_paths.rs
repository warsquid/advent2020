use std::fs::File;
use std::io::{BufRead, BufReader, Error, ErrorKind, Read};
use std::time::Instant;
use std::convert::TryInto;

#[derive(Debug)]
#[derive(Clone)]
struct TreeLine {
    contents: Vec<i64>,
    length: usize
}

// impl Iterator for TreeLine {
//     type Item = TreeLine;
//     fn next(&mut self) -> Option(Self::Item);
// }

fn has_tree(tree_line: &TreeLine, index: usize) -> bool {
    let wrapped_index = index % tree_line.length;
    let result = tree_line.contents[wrapped_index] == 1;
    return result;
}

fn read_input<R: Read>(io: R) -> Vec<TreeLine> {
    let br = BufReader::new(io);
    let tree_lines = br.lines()
        .map(|line| {
            let string = line.unwrap();
            let chars = string.chars();
            let trees: Vec<i64> = chars.map(|c|  if c == '#' {1} else {0})
                .collect();
            let len = trees.len();
            let tree = TreeLine {
                contents: trees,
                length: len
            };
            return tree;
        })
        .collect();
    return tree_lines;
}

fn main() {
    let tree_lines = read_input(File::open("data/03_actual.txt").unwrap());

    let mut tree_count = 0;
    let mut index = 0;
    for tree_line in tree_lines.iter() {
        if has_tree(&tree_line, index) {
            tree_count += 1;
        }
        index += 3;
    }
    println!("{}", tree_count);

    let steps = vec![1, 3, 5, 7, 1];
    let down_steps = vec![1, 1, 1, 1, 2];
    let mut total_trees = 0;
    let mut tree_counts: Vec<i64> = vec![];
    for (right_step, down_step) in steps.iter().zip(down_steps) {
        let mut tree_count = 0;
        let mut index = 0;
        for (line_index, tree_line) in tree_lines.iter().enumerate() {
            if (line_index % down_step) != 0 {
                continue;
            }
            if has_tree(&tree_line, index) {
                tree_count += 1;
            }
            index += right_step;
        }
        println!("{}", tree_count);
        total_trees += tree_count;
        tree_counts.push(tree_count);
    }
    println!("{}", total_trees);
    let answer2 = tree_counts.iter().fold(1, |a, b| a * b);
    println!("{}", answer2)
}