#![feature(str_split_once)]
use std::fs::File;
use std::io::{BufRead, BufReader, Error, ErrorKind, Read};
use std::collections::HashSet;
use std::collections::HashMap;
use regex::Regex;

#[derive(Debug)]
struct Passport {
    fields: Vec<String>,
    hash_map: HashMap<String, String>
}

fn get_required_fields() -> Vec<String> {
    vec![
        "byr".to_string(), 
        "iyr".to_string(), 
        "eyr".to_string(), 
        "hgt".to_string(), 
        "hcl".to_string(), 
        "ecl".to_string(), 
        "pid".to_string()
    ]
}

fn read_input<R: Read>(io: R) -> Vec<Passport> {
    let br = BufReader::new(io);
    let lines = br.lines();
    let mut passports: Vec<Passport> = Vec::new();
    let mut passport: Vec<String> = Vec::new();
    let mut hash_map: HashMap<String, String> = HashMap::new();
    for item in lines {
        let line = item.expect("Failed to read line");
        if ! line.trim().is_empty() {
            line.split(' ')
                .map(|s| {
                    let (field, value) = s.split_once(':').expect("Can't split on :");
                    return (field, value);
                })
                .for_each(|(f, v)| {
                    passport.push(f.to_string());
                    hash_map.insert(f.to_string(), v.to_string());
                });
        } else {
            passports.push(Passport { 
                fields: passport.iter().map(|x| x.to_string()).collect(),
                hash_map: hash_map
            });
            passport = Vec::new();
            hash_map = HashMap::new();
        }
    } 
    // Get final item
    if passport.len() > 0 {
        passports.push(Passport { 
            fields: passport.iter().map(|x| x.to_string()).collect(),
            hash_map: hash_map
        });
    }
    return passports;
}

fn is_valid(passport: &Passport) -> bool {
    let mut req_fields = HashSet::new();
    for field in &get_required_fields() {
        req_fields.insert(field.to_string());
    }
    let mut obs_fields = HashSet::new();
    for field in &passport.fields {
        obs_fields.insert(field.clone());
    }
    let diff: Vec<&String> = req_fields.difference(&obs_fields).collect();
    diff.is_empty()
}

fn has_valid_info(passport: &Passport) -> bool {
    fn get_year(passport: &Passport, field: &str) -> i64 {
        passport.hash_map.get(field)
                .expect("No birth year")
                .parse()
                .expect("Failed to parse number")
    }

    let birth_year = get_year(passport, "byr");
    if (birth_year < 1920) || (birth_year > 2002) {
        return false;
    }

    let issue_year = get_year(passport, "iyr");
    if (issue_year < 2010) || (issue_year > 2020) {
        return false;
    }

    let expiry_year = get_year(passport, "eyr");
    if (expiry_year < 2020) || (expiry_year > 2030) {
        return false;
    }

    // Use regex to check height: 150-193cm or 59-76in
    let height_pattern = Regex::new(r"^(1([5-8][0-9]|9[0-3])cm)|((59|6[0-9]|7[0-6])in)$")
        .unwrap();
    let height = passport.hash_map.get("hgt").expect("height not found");
    if ! height_pattern.is_match(height) {
        println!("Invalid height: {}", height);
        return false;
    }

    let hair_pattern = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
    if ! hair_pattern.is_match(passport.hash_map.get("hcl").unwrap()) {
        return false;
    }

    let eye_pattern = Regex::new(r"^(amb|blu|brn|gry|grn|hzl|oth)$")
        .unwrap();
    if ! eye_pattern.is_match(passport.hash_map.get("ecl").unwrap()) {
        return false;
    }

    let pid_pattern = Regex::new(r"^[0-9]{9}$").unwrap();
    if ! pid_pattern.is_match(passport.hash_map.get("pid").unwrap()) {
        return false;
    }

    return true;
}

fn main() {
    let passports = read_input(File::open("data/04_actual.txt").unwrap());
    //println!("{:?}", passports);
    println!("{}", passports.len());

    let results: Vec<bool> = passports.iter().map(|p| is_valid(p)).collect();
    println!("{:?}", results);
    let count = results.iter().fold(0, |a, b| a + (if *b {1} else {0}));
    println!("{}", count);

    let results2: usize = passports.iter()
        .filter(|p| is_valid(p))
        .filter(|p| has_valid_info(p))
        .count();
    println!("Part 2: {}", results2);
}
