use std::fs::File;
use std::io::{BufRead, BufReader, Read};


struct Grid {
    layout: Vec<Vec<i64>>,
    width: usize,
    height: usize
}

impl Grid {
    fn clone(&self) -> Grid {
        Grid {
            layout: self.layout.clone(),
            width: self.width,
            height: self.height
        }
    }

    fn get(&self, x: usize, y: usize) -> i64 {
        self.layout[y][x]
    }

    fn show(&self) {
        for row in self.layout.iter() {
            println!("{:?}", row);
        }
    }

    fn get_neighbours(&self, x: usize, y: usize) -> Vec<i64> {
        let offsets: Vec<i64> = vec![-1, 0, 1];
        let mut neighbours: Vec<i64> = Vec::new();
        for x_offset in offsets.iter() {
            let x_index = (x as i64) + x_offset;
            if (x_index < 0) || (x_index >= (self.width as i64)) {
                continue;
            }
            for y_offset in offsets.iter() {
                if (x_offset == &0) && (y_offset == &0) {
                    continue;
                }
                let y_index = (y as i64) + y_offset;
                if (y_index < 0) || (y_index >= (self.height as i64)) {
                    continue;
                }
                neighbours.push(self.get(x_index as usize, y_index as usize));
            }
        }
        neighbours
    }

    fn get_visible_neighbours(&self, x: usize, y: usize) -> Vec<i64> {
        let x_offsets: Vec<i64> = vec![-1, 0, 1];
        let y_offsets: Vec<i64> = vec![-1, 0, 1];
        let mut neighbours: Vec<i64> = Vec::new();
        for x_offset in x_offsets.iter() {
            for y_offset in y_offsets.iter() {
                if (x_offset == &0) && (y_offset == &0) {
                    continue;
                }
                let mut x_index = (x as i64) + x_offset;
                let mut y_index = (y as i64) + y_offset;

                loop {
                    let outside_x = (x_index < 0) || (x_index >= (self.width as i64));
                    let outside_y = (y_index < 0) || (y_index >= (self.height as i64)); 
                    if outside_x || outside_y {
                        break;
                    }
                    let current_visible = self.get(x_index as usize, y_index as usize);
                    if (current_visible == 1) || (current_visible == 2) {
                        neighbours.push(current_visible);
                        break;
                    }
                    x_index += x_offset;
                    y_index += y_offset;
                }
            }
        }
        neighbours
    }
}

impl PartialEq for Grid {
    fn eq(&self, other: &Self) -> bool {
        self.layout == other.layout
    }
}

fn get_new_state(grid: &Grid) -> Grid {
    let mut new_layout: Vec<Vec<i64>> = Vec::new();
    for y in 0..grid.height {
        let mut row: Vec<i64> = Vec::new();
        for x in 0..grid.width {
            let current_seat = grid.get(x, y);
            let neighbours = grid.get_neighbours(x, y);
            let occupied_count = neighbours.iter()
                .fold(0, |acc, val| if val == &2 {acc + 1} else {acc});

            if current_seat == 1 {
                if occupied_count == 0 {
                    row.push(2);
                } else {
                    row.push(1);
                }
            } else if current_seat == 2 {
                if occupied_count >= 4 {
                    row.push(1);
                } else {
                    row.push(2);
                }
            } else {
                row.push(current_seat);
            }
        }
        new_layout.push(row);
    }

    Grid {
        layout: new_layout,
        width: grid.width,
        height: grid.height
    }
}

fn read_input<R: Read>(io: R) -> Grid {
    let mut br = BufReader::new(io);
    let layout: Vec<String> = br.lines()
        .map(|x| x.unwrap().to_string())
        .collect();
    let layout_mat = layout.iter()
        .map(|row| {
            row.chars().map(|s| {
                match s {
                    'L' => 1,
                    '.' => 0,
                    '#' => 2,
                    _ => -1
                }
            })
            .collect()
        })
        .collect();
    let width = layout[0].len();
    let height = layout.len();
    Grid {
        layout: layout_mat,
        width: width,
        height: height
    }
}

fn get_new_state_part2(grid: &Grid) -> Grid {
    let mut new_layout: Vec<Vec<i64>> = Vec::new();
    for y in 0..grid.height {
        let mut row: Vec<i64> = Vec::new();
        for x in 0..grid.width {
            let current_seat = grid.get(x, y);
            let neighbours = grid.get_visible_neighbours(x, y);
            let occupied_count = neighbours.iter()
                .fold(0, |acc, val| if val == &2 {acc + 1} else {acc});

            if current_seat == 1 {
                if occupied_count == 0 {
                    row.push(2);
                } else {
                    row.push(1);
                }
            } else if current_seat == 2 {
                if occupied_count >= 5 {
                    row.push(1);
                } else {
                    row.push(2);
                }
            } else {
                row.push(current_seat);
            }
        }
        new_layout.push(row);
    }

    Grid {
        layout: new_layout,
        width: grid.width,
        height: grid.height
    }
}

fn main() {
    let mut grid = read_input(File::open("data/11_actual.txt").unwrap());

    for i in 0..10000 {
        let prev_grid = grid.clone();
        grid = get_new_state(&grid);

        if prev_grid == grid {
            println!("Equal after {} iterations", i);
            break;
        }
    }
    let row_counts: Vec<i64> = grid.layout.iter()
        .map(|row| {
            row.iter()
            .fold(0, |total, x| if x == &2 {total + 1} else {total})
        })
        .collect();
    let total_count = row_counts.iter()
        .fold(0, |total, x| total + x);
    println!("{}", total_count);

    let mut grid2 = read_input(File::open("data/11_actual.txt").unwrap());

    for i in 0..1000 {
        //grid2.show();
        let prev_grid2 = grid2.clone();
        grid2 = get_new_state_part2(&grid2);

        if prev_grid2 == grid2 {
            println!("Equal after {} iterations", i);
            break;
        }
    }
    let row_counts2: Vec<i64> = grid2.layout.iter()
        .map(|row| {
            row.iter()
            .fold(0, |total, x| if x == &2 {total + 1} else {total})
        })
        .collect();
    let total_count2 = row_counts2.iter()
        .fold(0, |total, x| total + x);
    println!("Part 2: {}", total_count2);
}