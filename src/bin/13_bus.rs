use std::fs::File;
use std::io::{BufRead, BufReader, Read};
use std::iter::FromIterator;

fn read_input<R: Read>(io: R) -> (i64, Vec<i64>) {
    let mut br = BufReader::new(io);
    let mut first_line = String::new();
    br.read_line(&mut first_line);
    let earliest: i64 = first_line.trim().parse().unwrap();

    let mut second_line = String::new();
    br.read_line(&mut second_line);
    let nums: Vec<String> = second_line.trim()
        .split(',')
        .filter(|s| s != &"x")
        .map(|s| s.to_string())
        .collect();
    let intervals: Vec<i64> = second_line.trim()
        .split(',')
        .filter(|s| s != &"x")
        .map(|s| s.parse().unwrap())
        .collect();
    (earliest, intervals)
}

fn main() {
    let (start_at, intervals) = read_input(File::open("data/13_actual.txt").unwrap());

    let ans1 = earliest_depart_time(start_at, &intervals);
    println!("Part 1: {}", ans1);
}

fn earliest_depart_time(start_at: i64, intervals: &Vec<i64>) -> i64 {
    let mut best_interval = intervals[0];
    let mut best_diff = best_interval;

    for interval in intervals {
        let diff = interval - (start_at % interval);
        if diff < best_diff {
            best_interval = *interval;
            best_diff = diff;
        }
    }
    println!("Interval: {}, Difference: {}", best_interval, best_diff);
    return best_interval * best_diff;
}