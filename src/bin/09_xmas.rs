use std::fs::File;
use std::io::{BufRead, BufReader, Read};
use std::collections::{HashMap, HashSet};
use std::convert::TryFrom;


fn main() {
    let example = false;
    let filename: &str;
    let preamble_size: i64;
    if example {
        filename = "data/09_example.txt";
        preamble_size = 5;
    } else {
        filename = "data/09_actual.txt";
        preamble_size = 25;
    }
    let input = read_input(File::open(filename).unwrap());

    let nums: Vec<i64> = input.iter()
        .map(|n| n.parse().expect("Couldn't parse number"))
        .collect();

    let ans = check_sums(&nums, preamble_size);
    println!("{}", ans);

    let ans2 = find_contiguous(&nums, ans);
    println!("{}", ans2);
}

fn check_sums(nums: &Vec<i64>, pre_size: i64) -> i64 {
    let mut sums: Vec<HashMap<i64, i64>> = nums.iter().map(|_x| HashMap::new()).collect();
    for index_usize in (0..nums.len()) {
        let index: i64 = i64::try_from(index_usize).unwrap();
        for offset in -pre_size..0 {
            if (index + offset) < 0 {
                continue;
            }
            let sum = nums[index as usize] + nums[(index + offset) as usize];
            sums[index as usize].insert(offset, sum);
        }
    }

    for i_usize in ((pre_size as usize)..nums.len()) {
        let mut found = false;
        let i: i64 = i64::try_from(i_usize).unwrap();
        let target_n = nums[i_usize];
        for offset in -pre_size..0 {
            let prev_index = (i + offset) as usize;
            for sub_offset in (-pre_size - offset)..0 {
                if sums[prev_index][&sub_offset] == target_n {
                    found = true;
                    break;
                }
            }
        }
        if ! found {
            return target_n;
        }
    }
    return -1;

}

fn find_contiguous(nums: &Vec<i64>, target: i64) -> i64 {
    let mut contig_numbers: Vec<i64> = Vec::new();
    for start_index in 0..nums.len() {
        contig_numbers = vec![nums[start_index]];

        let mut sum = nums[start_index];
        let mut offset: usize = 1;
        let mut searching = true;
        while searching {
            let new_num = nums[start_index + offset];
            sum += new_num;
            contig_numbers.push(new_num);
            if sum >= target {
                searching = false;
                break;
            }
            offset += 1;
        }
        if sum == target {
            break;
        }
    }
    let min = contig_numbers.iter().fold(contig_numbers[0], |a, b| if a < *b {a} else {*b});
    let max = contig_numbers.iter().fold(contig_numbers[0], |a, b| if a > *b {a} else {*b});
    return min + max;
}

fn read_input<R: Read>(io: R) -> Vec<String> {
    let br = BufReader::new(io);
    let lines = br.lines();
    lines.map(|x| x.unwrap().to_string())
        .collect()
}

