#![feature(str_split_once)]
use std::fs::File;
use std::io::{BufRead, BufReader, Error, ErrorKind, Read};
use std::collections::HashMap;
use std::collections::HashSet;
use regex::Regex;

#[derive(Debug)]
struct Bag {
    colour: String,
    contents: Vec<(String, i64)>
}

#[derive(Debug)]
struct Node {
    parents: Vec<String>,
    children: HashMap<String, i64>
}

fn read_input<R: Read>(io: R) -> Vec<Bag> {
    let bag_pattern = Regex::new(r"^\w+ \w+").unwrap();
    let contents_pattern = Regex::new(r"(\d+) (\w+ \w+) bag").unwrap();
    let br = BufReader::new(io);
    let lines = br.lines();
    let mut bags = Vec::new();
    for item in lines {
        let line = item.expect("Failed to read line");
        if ! line.trim().is_empty() {
            let (bag_description, contents_description) = line.split_once("contain")
                .expect("Couldn't split.");
            let bag_colour = bag_pattern.find(bag_description).unwrap().as_str();
            let contents: Vec<(String, i64)> = contents_pattern.captures_iter(contents_description)
                .map(|mat| {
                    let number: i64 = mat.get(1).map_or(0, |m| m.as_str().parse().unwrap());
                    let colour = mat.get(2).map_or("".to_string(), 
                                                   |m| m.as_str().to_string());
                    return (colour, number);
                })
                .collect();
            let current_bag = Bag{
                colour: bag_colour.to_string(),
                contents: contents
            };
            bags.push(current_bag);
        }
    }
    return bags;
}

fn build_tree(rules: Vec<Bag>) -> HashMap<String, Node> {
    let mut tree = HashMap::new();
    for bag in rules.iter() {
        let mut bag_node = Node {
            parents: Vec::new(),
            children: HashMap::new()
        }; 
        bag.contents.iter()
            .for_each(|(colour, number)| {
                bag_node.children.insert(colour.to_string(), *number);
            });
        tree.insert(bag.colour.to_string(), bag_node);
    }
    for bag in rules.iter() {
        for (child, number) in bag.contents.iter() {
            tree.get_mut(child).unwrap().parents.push(bag.colour.to_string());
        }
    }

    return tree;
}

fn get_all_parents(bag: &str, tree: &HashMap<String, Node>) -> HashSet<String> {
    let mut seen: HashSet<String> = HashSet::new();
    let mut queue: Vec<String> = Vec::new();

    let mut current_node = &bag;
    for node in &tree[&bag.to_string()].parents {
        seen.insert(node.to_string());
        queue.push(node.to_string());
    }

    while queue.len() > 0 {
        let current_node = &queue.remove(0);
        for node in &tree[current_node].parents {
            if ! seen.contains(node) {
                seen.insert(node.to_string());
                queue.push(node.to_string());
            }
        }
    }

    return seen;
}

fn get_all_children(bag: &str, tree: &HashMap<String, Node>) -> i64 {
    println!("{}", bag);
    let mut total = 0;
    let mut current_node = &bag;
    for (child, quantity) in &tree[&bag.to_string()].children {
        total += quantity;
        total += quantity * get_all_children(child, tree);
    }
    return total;
}

fn main() {
    let input = read_input(File::open("data/07_actual.txt").unwrap());
    // println!("{:?}", input);

    let tree = build_tree(input);
    // println!("{:?}", tree);

    let all_parents = get_all_parents("shiny gold", &tree);
    // println!("{:?}", all_parents);
    println!("{}", all_parents.len());

    let total_children = get_all_children("shiny gold", &tree);
    println!("{}", total_children);
}
