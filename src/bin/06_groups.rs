use std::fs::File;
use std::hash::Hash;
use std::io::{BufRead, BufReader, Error, ErrorKind, Read};
use std::collections::HashSet;

fn read_input<R: Read>(io: R) -> Vec<HashSet<String>> {
    let br = BufReader::new(io);
    let lines = br.lines();
    let mut groups = Vec::new();
    let mut answer_set = HashSet::new();
    for item in lines {
        let line = item.expect("Failed to read line");
        if ! line.trim().is_empty() {
            line.chars()
                .for_each(|c| {
                    answer_set.insert(c.to_string());
                });
        } else {
            groups.push(answer_set);
            answer_set = HashSet::new();
        }
    } 
    // Get final item
    if answer_set.len() > 0 {
        groups.push(answer_set);
    }
    return groups;
}

fn inplace_intersection<T>(a: &mut HashSet<T>, b: &mut HashSet<T>) -> HashSet<T>
where
    T: Hash,
    T: Eq,
{
    let c: HashSet<T> = a.iter().filter_map(|v| b.take(v)).collect();
    
    a.retain(|v| !c.contains(&v));

    c
}

fn read_input_part2<R: Read>(io: R) -> Vec<HashSet<String>> {
    let br = BufReader::new(io);
    let lines = br.lines();
    let mut groups = Vec::new();
    let mut answer_set = HashSet::new();
    let mut new_group = true;
    for item in lines {
        let line = item.expect("Failed to read line");
        if ! line.trim().is_empty() {
            if new_group {
                line.chars()
                    .for_each(|c| {
                        answer_set.insert(c.to_string());
                    });
                new_group = false;
            } else {
                let mut new_person: HashSet<String> = HashSet::new();
                line.chars()
                    .for_each(|c| {
                        new_person.insert(c.to_string());
                });
                answer_set = inplace_intersection(&mut answer_set, &mut new_person);
            }
        } else {
            groups.push(answer_set);
            answer_set = HashSet::new();
            new_group = true;
        }
    } 
    // Get final item
    if answer_set.len() > 0 {
        groups.push(answer_set);
    }
    return groups;
}

fn main() {
    let input = read_input(File::open("data/06_actual.txt").unwrap());
    // println!("{:?}", input);

    let total = input.iter()
        .map(|g| g.len())
        .fold(0, |a, b| a + b);
    println!("{}", total);

    let p2_input = read_input_part2(File::open("data/06_actual.txt").unwrap());

    let p2_total = p2_input.iter()
        .map(|g| g.len())
        .fold(0, |a, b| a + b);
    println!("{}", p2_total);
}