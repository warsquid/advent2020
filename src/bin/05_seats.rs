use std::fs::File;
use std::io::{BufRead, BufReader, Error, ErrorKind, Read};

fn read_input<R: Read>(io: R) -> Vec<String> {
    let br = BufReader::new(io);
    br.lines()
        .map(|line| line.unwrap().to_string())
        .collect()
}

fn get_seat(code: &str) -> (i64, i64) {
    let row_code = code[0..7].to_string();
    let col_code = code[7..].to_string();

    let mut row_lower = 0;
    let mut row_upper = 127;

    for item in row_code.chars().take(6) {
        if item == 'F' {
            row_upper = row_upper - ((row_upper - row_lower) / 2) - 1;
        } else {
            row_lower = row_lower + ((row_upper - row_lower) / 2) + 1;
        }
    }
    let final_row: i64;
    if row_code.chars().nth(6).unwrap() == 'F' {
        final_row = row_lower;
    } else {
        final_row = row_upper;
    }
    // println!("Row: {}", final_row);

    let mut col_lower = 0;
    let mut col_upper = 7;

    for item in col_code.chars().take(2) {
        if item == 'L' {
            col_upper = col_upper - ((col_upper - col_lower) / 2) - 1;
        } else {
            col_lower = col_lower + ((col_upper - col_lower) / 2) + 1;
        }
    }
    let final_col: i64;
    if col_code.chars().nth(2).unwrap() == 'L' {
        final_col = col_lower;
    } else {
        final_col = col_upper;
    }
    // println!("Column: {}", final_col);
    return (final_row, final_col);
}

fn main() {
    let input = read_input(File::open("data/05_actual.txt").unwrap());
    println!("{:?}", input);

    let hightest_code: i64 = input.iter()
        .map(|s| get_seat(s))
        .map(|(row, col)| (8 * row) + col)
        .fold(0, |a, b| if a > b {a} else {b});
    println!("{}", hightest_code);


    let mut seat_ids: Vec<i64> = input.iter()
        .map(|s| get_seat(s))
        .map(|(row, col)| (8 * row) + col)
        .collect();
    seat_ids.sort();
    println!("{:?}", seat_ids);
    let mut prev_seat = 0;
    for seat in seat_ids {
        if (seat - prev_seat) > 1 {
            println!("{}", prev_seat);
        }
        prev_seat = seat;
    }
}

